#include <stdio.h>
#include <stdlib.h>
int main()
{
    FILE *f;
    f = fopen("homelistings.csv","r");
    if(!f)
    {
        printf("cant open the file");
        exit(1);
    }
    
    FILE *s;
    s = fopen("small.txt", "w");
    if(!s)
    {
        printf("can't open the file");
        exit(1);
    }
    FILE *m;
    m = fopen("med.txt", "w");
    if(!m)
    {
        printf("can't open the file");
        exit(1);
    }
    FILE *l;
    l = fopen("large.txt", "w");
    if(!l)
    {
        printf("can't open the file");
        exit(1);
    }
    int zip,id,price, bedrooms, bathrooms, area;
    char address[40];
    
    while(fscanf(f,"%d,%d,%[^,],%d,%d,%d,%d", &zip, &id, address,&price, &bedrooms, &bathrooms, &area) != EOF)
    {
        if(area >=1000 && area <= 2000)
        {
            fprintf(m, "%s : %d\n", address, area);
        }
        if(area <1000)
        {
            fprintf(s, "%s : %d\n", address, area);
        }
        if(area >2000)
        {
            fprintf(l, "%s : %d\n", address, area);
        }
    }
}